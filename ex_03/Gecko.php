<?php

class Gecko
{
    private $name;

    public function __construct($n = null)
    {
        if ($n)
            echo "Hello $n !\n";
        else
            echo "Hello !\n";
        $this->name = $n;
    }

    public function __destruct()
    {
        if ($this->name)
            echo "Bye $this->name !\n";
        else
            echo "Bye !\n";
    }

    public function getName()
    {
        return $this->name;
    }
};